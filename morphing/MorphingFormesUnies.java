package morphing;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import application.abstraction.DonneesMorphingAvecPointsControleSimples;
import application.abstraction.PointControleSimple;

/**
 * Cette classe abstraite g�n�ralise les m�thodes communes � tous les morphings de formes unies.
 */
public abstract class MorphingFormesUnies extends Morphing<PointControleSimple> {
    protected RGB couleurFond;
    protected RGB couleurForme;
    
    /**
     * Constructeur de la classe MorphingFormesUnies.
     * @param donnee DonneesMorphingAvecPointsControleSimples Les donn�es de morphing avec des points de contr�le simples.
     * @param nombreImagesIntermediaires int Le nombre d'images interm�diaires pour le morphing.
     */
    public MorphingFormesUnies(DonneesMorphingAvecPointsControleSimples donnee, int nombreImagesIntermediaires) {
        super(donnee, nombreImagesIntermediaires);
        
        this.couleurFond = null;
        this.couleurForme = null;
        RGB[][] imageDepart = getImageDonneesRGB(0);
        
        // R�cup�ration de la couleur du fond et la couleur de la forme sur la premi�re image
        boucleExterne:
        for (int i = 0; i < imageDepart.length; i++) {
            for (int j = 0; j < imageDepart[i].length; j++) {
                RGB couleurPixel = imageDepart[i][j];
                
                if (couleurFond == null) {
                    couleurFond = couleurPixel;
                } else if (!couleurPixel.equals(couleurFond)) {
                    couleurForme = couleurPixel;
                    break boucleExterne;
                }
            }
        }
    }
    
    /**
     * Obtient la couleur du fond.
     */
    private RGB getCouleurFond() {
        return this.couleurFond;
    }
    
    /**
     * Obtient la couleur de la forme.
     */
    private RGB getCouleurForme() {
        return this.couleurForme;
    }
    
    /**
     * Permet � partir du polygone form� par des points de contr�le simples de cr�er l'image qui contient ce polygone.
     * @param listePointsControleSimples ArrayList<PointControleSimple> repr�sentant le polygone, le dernier point doit �tre �gal au premier.
     * @param largeur int largeur de l'image g�n�r�e.
     * @param hauteur int hauteur de l'image g�n�r�e.
     * @return RGB[][] contenant le polygone trac�.
     */
    private RGB[][] genererImageAvecListePointsControleSimples(ArrayList<PointControleSimple> listePointsControleSimples, int largeur, int hauteur) {
        // D�termine les coordonn�es minimales et maximales du polygone
        int yMax = (int) Math.round(listePointsControleSimples.get(0).getY());
        int yMin = (int) Math.round(listePointsControleSimples.get(0).getY());
        int xMax = (int) Math.round(listePointsControleSimples.get(0).getX());
        int xMin = (int) Math.round(listePointsControleSimples.get(0).getX());
        
        for (PointControleSimple p : listePointsControleSimples) {
            if ((int) Math.round(p.getY()) > yMax) {
                yMax = (int) Math.round(p.getY());
            }
            
            if ((int) Math.round(p.getY()) < yMin) {
                yMin = (int) Math.round(p.getY());
            }
            
            if ((int) Math.round(p.getX()) > xMax) {
                xMax = (int) Math.round(p.getX());
            }
            
            if ((int) Math.round(p.getX()) < xMin) {
                xMin = (int) Math.round(p.getX());
            }
        }
        
        // Remplit l'image r�sultat de la couleur du fond
        RGB[][] imageResultat = new RGB[largeur][hauteur];
        for (int i = 0; i < largeur; i++) {
            for (int j = 0; j < hauteur; j++) {
                imageResultat[i][j] = this.getCouleurFond();
            }
        }
        
        // Trace les contours du polygone
        tracerContoursForme(listePointsControleSimples, imageResultat);
        
        // Colore l'int�rieur de la forme ainsi dessin�e
        boolean vientDeTomberSurUnPixelColorieDeLaForme = false;
        boolean estDansForme = false;
        for (int i = xMin; i <= xMax; i++) {
            vientDeTomberSurUnPixelColorieDeLaForme = false;
            estDansForme = false;
            
            for (int j = yMin; j < yMax; j++) {
                // S�rie de v�rifications pour limiter l'appel de pointEstDansPolygone, ce qui all�ge grandement la complexit�
                if (vientDeTomberSurUnPixelColorieDeLaForme && !estDansForme) {
                    estDansForme = true;
                }
                
                if (estDansForme && !imageResultat[i][j].equals(getCouleurForme())) {
                    if (pointEstDansPolygone(listePointsControleSimples, i, j)) {
                        // Appel d'un remplissage r�cursif quand on est dans la forme et que la zone n'a pas �t� colori�e
                        remplirRecursivementForme(imageResultat, i, j, xMin, xMax, yMin, yMax);
                    } else {
                        estDansForme = false;
                    }
                }
                
                vientDeTomberSurUnPixelColorieDeLaForme = imageResultat[i][j].equals(getCouleurForme());
            }
        }
        
        return imageResultat;
    }
	
    /**
     * Dessine les contours du polygone sur l'image.
     * @param listePointsControleSimples ArrayList<PointControleSimple> repr�sentant le polygone, le dernier point doit �tre �gal au premier.
     * @param img RGB[][] sur laquelle le polygone sera dessin�.
     */
    private void tracerContoursForme(ArrayList<PointControleSimple> listePointsControleSimples, RGB[][] img) {
        for (int i = 1; i < listePointsControleSimples.size(); i++) {
            PointControleSimple pointControleDepart = listePointsControleSimples.get(i - 1);
            PointControleSimple pointControleArrivee = listePointsControleSimples.get(i);
            tracerLigneBresenham((int) Math.round(pointControleDepart.getX()), (int) Math.round(pointControleDepart.getY()), (int) Math.round(pointControleArrivee.getX()), (int) Math.round(pointControleArrivee.getY()), img);
        }
    }

    /**
     * Trace la ligne partant de (x0, y0) et allant � (x1, y1) sur l'image, s'appuie sur l'algorithme de Bresenham.
     * @param x0 int coordonn�e x du premier point du segment � colorier.
     * @param y0 int coordonn�e y du premier point du segment � colorier.
     * @param x1 int coordonn�e x du deuxi�me point du segment � colorier.
     * @param y1 int coordonn�e y du deuxi�me point du segment � colorier.
     * @param image RGB[][] sur laquelle le segment sera dessin�.
     */
    private void tracerLigneBresenham(int x0, int y0, int x1, int y1, RGB[][] image) {
        // R�cup�ration des dimensions de l'image
        int largeurImage = image.length;
        int hauteurImage = (image.length > 0) ? image[0].length : 0;

        // Calcul des deltas et pas pour l'algorithme de Bresenham
        int deltaX = Math.abs(x1 - x0);
        int deltaY = Math.abs(y1 - y0);
        int pasX = x0 < x1 ? 1 : -1;
        int pasY = y0 < y1 ? 1 : -1;
        int erreur = deltaX - deltaY;

        // Parcours des pixels selon l'algorithme de Bresenham
        while (true) {
            // Coloration du pixel si ses coordonn�es sont dans l'image
            if (coordonneesDansImage(x0, y0, largeurImage, hauteurImage)) {
                image[x0][y0] = getCouleurForme();
            }

            // Arr�t de la boucle lorsque le dernier point est atteint
            if (x0 == x1 && y0 == y1) {
                break;
            }
            
            
            // Mise � jour de l'erreur selon les deltas et les pas
            int doubleErreur = 2 * erreur;
            if (doubleErreur > -deltaY) {
            	erreur -= deltaY;
                x0 += pasX;
            }
            if (doubleErreur < deltaX) {
            	erreur += deltaX;
                y0 += pasY;
            }
        }
    }

    /**
     * V�rifie si les coordonn�es sp�cifi�es sont � l'int�rieur des dimensions de l'image.
     * @param coordonneeX int coordonn�e x.
     * @param coordonneeY int coordonn�e y.
     * @param largeur int largeur de l'image.
     * @param hauteur int hauteur de l'image.
     * @return true si les coordonn�es sont � l'int�rieur de l'image, false sinon.
     */
    private Boolean coordonneesDansImage(int coordonneeX, int coordonneeY, int largeur, int hauteur) {
        return coordonneeX >= 0 && coordonneeX < largeur && coordonneeY >= 0 && coordonneeY < hauteur;
    }
	
    /**
     * Remplit r�cursivement la forme � partir d'un point en utilisant la technique de remplissage par diffusion.
     * @param image RGB[][] Image sur laquelle la forme sera remplie.
     * @param x0 int Coordonn�e x du point de d�part du remplissage.
     * @param y0 int Coordonn�e y du point de d�part du remplissage.
     * @param xMin int Minimum de coordonn�e en x coloriable.
     * @param xMax int Maximum de coordonn�e en x coloriable.
     * @param yMin int Minimum de coordonn�e en y coloriable.
     * @param yMax int Maximum de coordonn�e en y coloriable.
     */
    private void remplirRecursivementForme(RGB[][] image, int x0, int y0, int xMin, int xMax, int yMin, int yMax) {
        LinkedList<Integer> listeCoordonneesX = new LinkedList<>();
        LinkedList<Integer> listeCoordonneesY = new LinkedList<>();
        listeCoordonneesX.add(x0);
        listeCoordonneesY.add(y0);

        // Parcours r�cursif des pixels adjacents
        while (!listeCoordonneesX.isEmpty()) {
            int i = listeCoordonneesX.getFirst();
            int j = listeCoordonneesY.getFirst();
            // V�rification des limites de l'image et de la couleur du pixel
            if (i >= xMin && i <= xMax && j >= yMin && j <= yMax && !image[i][j].equals(getCouleurForme())) {
                image[i][j] = getCouleurForme();

                // Ajout des coordonn�es des pixels adjacents � la liste
                listeCoordonneesX.add(i - 1);
                listeCoordonneesY.add(j);

                listeCoordonneesX.add(i + 1);
                listeCoordonneesY.add(j);

                listeCoordonneesX.add(i);
                listeCoordonneesY.add(j - 1);

                listeCoordonneesX.add(i);
                listeCoordonneesY.add(j + 1);
            }
            // Retrait des coordonn�es trait�es de la liste
            listeCoordonneesX.remove();
            listeCoordonneesY.remove();
        }
    }
    
    @Override
    protected RGB[][] genererKiemeImageEntreDeuxImages(int k, int indiceImageDepart, int indiceImageArrivee) {
        ArrayList<PointControleSimple> pointsControleKiemeImageResultat = new ArrayList<>();
        Iterator<PointControleSimple> iterateurImageDepart = this.getIterateurListePointsControleImage(indiceImageDepart);
        Iterator<PointControleSimple> iterateurImageArrivee = this.getIterateurListePointsControleImage(indiceImageArrivee);

        // Calcul des points de contr�le interm�diaires
        while (iterateurImageDepart.hasNext() && iterateurImageArrivee.hasNext()) {
            PointControleSimple pointControleImageDepart = iterateurImageDepart.next();
            PointControleSimple pointControleImageArrivee = iterateurImageArrivee.next();
            // Obtention du k-i�me point interm�diaire entre les deux points de contr�le
            PointControleSimple pointControleIntermediaire = PointControleSimple.getKiemePointEntreDeuxPoints(pointControleImageDepart, pointControleImageArrivee, getNombreImagesIntermediaires() + 1, k);
            pointsControleKiemeImageResultat.add(pointControleIntermediaire);
        }
        // G�n�ration de l'image avec les points de contr�le interm�diaires
        return genererImageAvecListePointsControleSimples(pointsControleKiemeImageResultat, (int) this.getImageResultat(0).getWidth(), (int) this.getImageResultat(0).getHeight());
    }
	
    /**
     * Fonction v�rifiant si un point est dans un polygone.
     * @param polygone ArrayList<PointControleSimple> Repr�sentant le polygone, le dernier point doit �tre �gal au premier.
     * @param coordonneeX int Coordonn�e x du point dont on veut v�rifier l'appartenance.
     * @param coordonneeY int Coordonn�e y du point dont on veut v�rifier l'appartenance.
     * @return boolean Signalant si oui ou non le point appartient au polygone.
     */
	protected boolean pointEstDansPolygone(ArrayList<PointControleSimple> polygone, int coordonneeX, int coordonneeY) {
	    int nombrePointsControleSimples = polygone.size();
	    boolean estDansPolygone = false;
	
	    // Parcourt des segments du polygone pour v�rifier l'appartenance du point
	    for (int i = 0, j = 1; j < nombrePointsControleSimples; i = j++) {
	        PointControleSimple pi = polygone.get(i);
	        PointControleSimple pj = polygone.get(j);
	        pi.setX((int) Math.round(pi.getX()));
	        pi.setY((int) Math.round(pi.getY()));
	        pj.setX((int) Math.round(pj.getX()));
	        pj.setY((int) Math.round(pj.getY()));
	
	        // V�rification de l'appartenance du point au segment du polygone
	        if ((pi.getY() > coordonneeY) != (pj.getY() > coordonneeY) && (coordonneeX < (pj.getX() - pi.getX()) * (coordonneeY - pi.getY()) / (pj.getY() - pi.getY()) + pi.getX())) {
	            estDansPolygone = !estDansPolygone;
	        }
	    }
	
	    return estDansPolygone;
	}
	
	@Override
	public void genererImagesResultat() {
		// G�n�ration des images interm�diaires entre les images de donn�es RGB
		for(int i = 1; i < this.getNombreImagesDonneesRGB(); i++) {
			for(int k = 1; k <= getNombreImagesIntermediaires(); k++) {
				this.setImageResultat((i-1)*getNombreImagesIntermediaires()+(i-1)+k, transformerImageRGBEnImage(genererKiemeImageEntreDeuxImages(k, i-1, i)));
			}
		}
		
		// G�n�ration des images en elles-m�mes � la main
		for(int i = 0; i < this.getNombreImagesDonneesRGB(); i++) {
			int k = i*getNombreImagesIntermediaires()+i;
			setImageResultat(k, transformerImageRGBEnImage(genererImageAvecListePointsControleSimples(getPointsDeControleImage(i), (int) this.getImageResultat(k).getWidth(), (int) this.getImageResultat(k).getHeight())));
		}
	}
}
