package exceptions;

/**
 * Cette exception est lev�e lorsqu'aucune image n'est s�lectionn�e.
 */
public final class AucuneImageSelectionneeException extends RuntimeException {
    private static final long serialVersionUID = 4921576089069319502L;

    /**
     * Constructeur prenant en param�tre un message d'erreur.
     * @param msg Le message d'erreur associ� � cette exception.
     */
    public AucuneImageSelectionneeException(String msg) {
        super(msg);
    }
}