package application.abstraction;

import java.util.LinkedList;
import application.controle.Observateur;

/**
 * Classe abstraite repr�sentant un objet observ� par des observateurs.
 */
public abstract class Observable {
	protected LinkedList<Observateur> listeObservateurs;
	
	/**
     * Constructeur de classe initialisant la liste des observateurs (� vide).
     */
	public Observable() {
		listeObservateurs = new LinkedList<Observateur>();
	}
	
	/**
     * Notifie tous les observateurs apr�s une modification de l'objet observ�.
     * @param typeDeModification Le type de modification effectu�e.
     */
	protected void notifierObservateurs(Integer typeDeModification) {
		for (Observateur observateur : listeObservateurs) {
			observateur.traitementApresModification(this, typeDeModification);
		}
	}
	
	/**
     * Retourne la liste des observateurs de l'objet observ�.
     * @return La liste des observateurs.
     */
	public LinkedList<Observateur> getListeObservateurs() {
		return listeObservateurs;
	}
}
