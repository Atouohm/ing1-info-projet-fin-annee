package application.abstraction;

/**
 * Interface d�finissant les propri�t�s d'un point de contr�le.
 */
public interface PointControle {

    /**
     * Cr�e une copie du point de contr�le.
     * @return La copie du point de contr�le.
     */
    public PointControle copie();

    /**
     * Harmonise le point de contr�le pass� en param�tre avec ce point de contr�le selon un seuil d'�quivalence donn�.
     * @param point Le point de contr�le � harmoniser.
     * @param seuilEquivalence Le seuil d'�quivalence pour l'harmonisation.
     */
    public void harmoniser(PointControle point, double seuilEquivalence);
}