package application.abstraction;

import java.util.ArrayList;
import exceptions.AucuneImageSelectionneeException;
import exceptions.ParametresInvalidesException;
import exceptions.TentativeAccesElementInexistantException;
import javafx.scene.image.Image;

/**
 * Classe abstraite repr�sentant les donn�es utilis�es dans le processus de morphing.
 */
public abstract class DonneesMorphing<T extends PointControle> extends Observable {
    public static final Integer CHANGEMENT_IMAGES = 1;
    public static final Integer CHANGEMENT_POINTS_DE_CONTROLE = 2;
    public static final Integer CHANGEMENT_INDICE_IMAGE_ACTUELLE = 3;

    private ArrayList<Image> images;
    protected ArrayList<ArrayList<T>> pointsControleDesImages;
    private int indiceImageActuelle;

    /**
     * Constructeur de classe initialisant les listes d'images et de points de contr�le.
     */
    public DonneesMorphing() {
        images = new ArrayList<>();
        pointsControleDesImages = new ArrayList<>();
        indiceImageActuelle = -1; // Initialis� � -1 car aucune image n'est s�lectionn�e � l'instanciation
    }

    /**
     * Retourne l'image s�lectionn�e/courante.
     * @return L'image s�lectionn�e/courante
     */
    public Image getImageActuelle() {
        return getImage(getIndiceImageActuelle());
    }

    /**
     * Retourne l'image de l'indice sp�cifi�.
     * @param indiceImage L'indice de l'image � r�cup�rer.
     * @return L'image � l'indice sp�cifi�.
     * @throws TentativeAccesElementInexistantException Si l'indice de l'image est invalide.
     */
    public Image getImage(int indiceImage) {
        if (indiceImage >= 0 && indiceImage < images.size()) {
            return images.get(indiceImage);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }

    /**
     * Retourne le point de contr�le de l'image s�lectionn�e � l'indice sp�cifi�.
     * @param indicePointControle L'indice du point de contr�le � r�cup�rer.
     * @return Le point de contr�le � l'indice sp�cifi� de l'image s�lectionn�e.
     */
    public T getPointControleImageActuelle(int indicePointControle) {
        return getPointControleImage(getIndiceImageActuelle(), indicePointControle);
    }

    /**
     * Retourne le point de contr�le de l'image � l'indice sp�cifi�.
     * @param indiceImage L'indice de l'image.
     * @param indicePointControle L'indice du point de contr�le � r�cup�rer.
     * @return Le point de contr�le � l'indice sp�cifi� de l'image � l'indice sp�cifi�.
     */
    public abstract T getPointControleImage(int indiceImage, int indicePointControle);

    /**
     * Retourne le nombre total d'images dans les donn�es de morphing.
     * @return Le nombre total d'images.
     */
    public int getNombreImages() {
        return images.size();
    }
    
    /**
     * Retourne le nombre total de points de contr�le par image.
     * @return Le nombre total de points de contr�le par image.
     */
    public int getNombrePointsControle() {
        if (pointsControleDesImages.size() == 0) {
            return 0;
        } else {
            // Bas� sur le nombre de points de contr�le de l'image d'indice 0, car toutes les images ont le m�me nombre de points de contr�le
            return pointsControleDesImages.get(0).size();
        }
    }
    
    /**
     * Retourne l'indice de l'image s�lectionn�e/courante.
     * @return L'indice de l'image s�lectionn�e/courante.
     * @throws AucuneImageSelectionneeException Si aucune image n'est s�lectionn�e.
     */
    public int getIndiceImageActuelle() {
        if (uneImageEstSelectionnee()) {
            return indiceImageActuelle;
        } else {
            throw new AucuneImageSelectionneeException("Aucune image n'est s�lectionn�e");
        }
    }

    /**
     * D�finit l'image actuellement s�lectionn�e.
     * @param urlImage L'URL de l'image � d�finir.
     * @throws ParametresInvalidesException Si l'URL de l'image est invalide.
     */
    public void setImageActuelle(String urlImage) {
        if (urlImage != null) {
            images.set(getIndiceImageActuelle(), new Image(urlImage));
            if (getNombreImages() == 1) {
                pointsControleDesImages.get(0).clear();
            }
            notifierObservateurs(CHANGEMENT_IMAGES);
        } else {
            throw new ParametresInvalidesException("L'image actuelle ne peut pas �tre d�finie avec une URL inexistante");
        }
    }

    /**
     * D�finit le point de contr�le de l'image actuellement s�lectionn�e � l'indice sp�cifi�.
     * @param indicePointControle L'indice du point de contr�le � d�finir.
     * @param pointControle Le point de contr�le � d�finir.
     * @param seuilEquivalence Le seuil d'�quivalence pour harmoniser les points de contr�le.
     * @throws TentativeAccesElementInexistantException Si l'indice de point de contr�le est invalide.
     */
    public void setPointControleImageActuelle(int indicePointControle, T pointControle, double seuilEquivalence) {
        if (pointControle != null && indicePointControle >= 0 && indicePointControle < getNombrePointsControle()) {
            for (int i = 0 ; i < getNombrePointsControle() ; i++) {
                if (i != indicePointControle) {
                    getPointControleImageActuelle(i).harmoniser(pointControle, seuilEquivalence);
                }
            }
            pointsControleDesImages.get(getIndiceImageActuelle()).set(indicePointControle, pointControle);
            notifierObservateurs(CHANGEMENT_POINTS_DE_CONTROLE);
        } else {
            throw new TentativeAccesElementInexistantException("Point de contr�le inexistant");
        }
    }

    /**
     * D�finit l'indice de l'image actuellement s�lectionn�e.
     * @param nouvelIndiceImageSelectionnee Le nouvel indice de l'image � s�lectionner.
     * @throws TentativeAccesElementInexistantException Si l'indice de l'image est invalide.
     */
    public void setIndiceImageActuelle(int nouvelIndiceImageSelectionnee) {
        if (nouvelIndiceImageSelectionnee >= 0 && nouvelIndiceImageSelectionnee < getNombreImages()) {
            indiceImageActuelle = nouvelIndiceImageSelectionnee;
            notifierObservateurs(CHANGEMENT_INDICE_IMAGE_ACTUELLE);
        } else {
            throw new TentativeAccesElementInexistantException("Image inexistante");
        }
    }

    /**
     * Ajoute une nouvelle image � la liste des images.
     * @param urlImage L'URL de l'image � ajouter.
     * @throws ParametresInvalidesException Si l'URL de l'image est invalide.
     */
    public void ajouterImage(String urlImage) {
        if (urlImage != null) {
            if (getNombreImages() > 0) {
                pointsControleDesImages.add(copierListePointsControle(pointsControleDesImages.get(getNombreImages() - 1)));
            } else {
                pointsControleDesImages.add(new ArrayList<>());
            }
            images.add(new Image(urlImage));
            indiceImageActuelle = getNombreImages() - 1;
            notifierObservateurs(CHANGEMENT_IMAGES);
        } else {
            throw new ParametresInvalidesException("L'image doit �tre d�finie pour l'ajouter");
        }
    }

    /**
     * Copie la liste des points de contr�le d'une image.
     * @param arrayList La liste des points de contr�le � copier.
     * @return La copie de la liste des points de contr�le.
     */
    protected abstract ArrayList<T> copierListePointsControle(ArrayList<T> arrayList);

    /**
     * Ajoute un point de contr�le � l'image s�lectionn�e/courante.
     * @param pointControle Le point de contr�le � ajouter.
     * @param seuilEquivalence Le seuil d'�quivalence pour harmoniser les points de contr�le.
     * @throws ParametresInvalidesException Si le point de contr�le est nul.
     */
    public void ajouterPointControleImageActuelle(T pointControle, double seuilEquivalence) {
        if (pointControle != null) {
            for (T point : pointsControleDesImages.get(getIndiceImageActuelle())) {
                point.harmoniser(pointControle, seuilEquivalence);
            }
            for (ArrayList<T> pointsControleImage : pointsControleDesImages) {
                pointsControleImage.add(pointControle);
            }
            notifierObservateurs(CHANGEMENT_POINTS_DE_CONTROLE);
        } else {
            throw new ParametresInvalidesException("Le point de contr�le doit �tre d�fini pour l'ajouter");
        }
    }

    /**
     * Supprime l'image s�lectionn�e/courante.
     */
    public void supprimerImageActuelle() {
        pointsControleDesImages.remove(getIndiceImageActuelle());
        images.remove(getIndiceImageActuelle());
        if (images.size() >= 1) {
            indiceImageActuelle = 0;
        } else {
            indiceImageActuelle = -1;
        }
        notifierObservateurs(CHANGEMENT_IMAGES);
    }

    /**
     * Supprime un point de contr�le sp�cifi� par son indice.
     * @param indicePointControle L'indice du point de contr�le � supprimer.
     * @throws TentativeAccesElementInexistantException Si l'indice du point de contr�le est invalide.
     */
    public void supprimerPointControle(int indicePointControle) {
        if (indicePointControle >= 0 && indicePointControle < getNombrePointsControle()) {
            for (ArrayList<T> pointsControleImage : pointsControleDesImages) {
                pointsControleImage.remove(indicePointControle);
            }
            notifierObservateurs(CHANGEMENT_POINTS_DE_CONTROLE);
        } else {
            throw new TentativeAccesElementInexistantException("Point de contr�le inexistant");
        }
    }

    /**
     * Supprime toutes les images.
     */
    public void supprimerToutesLesImages() {
        images.clear();
        indiceImageActuelle = -1;
        notifierObservateurs(CHANGEMENT_IMAGES);
    }

    /**
     * Supprime tous les points de contr�le de toutes les images.
     */
    public void supprimerTousLesPointsControle() {
        for (ArrayList<T> pointsControleImage : pointsControleDesImages) {
            pointsControleImage.clear();
        }
        notifierObservateurs(CHANGEMENT_POINTS_DE_CONTROLE);
    }

    /**
     * V�rifie si une image est actuellement s�lectionn�e.
     * @return true si une image est s�lectionn�e, sinon false.
     */
    public Boolean uneImageEstSelectionnee() {
        return indiceImageActuelle >= 0 && indiceImageActuelle < images.size();
    }

    /**
     * D�cale d'un pas l'image s�lectionn�e/courante vers le d�but de la liste des images.
     */
    public void decalerImageActuelleVersDebut() {
        deplacerImageActuelle(getIndiceImageActuelle() - 1);
    }

    /**
     * D�cale d'un pas l'image s�lectionn�e/courante vers la fin de la liste des images.
     */
    public void decalerImageActuelleVersFin() {
        deplacerImageActuelle(getIndiceImageActuelle() + 1);
    }

    /**
     * D�place l'image actuellement s�lectionn�e vers l'indice sp�cifi�.
     * @param nouvelIndice L'indice de destination.
     * @throws TentativeAccesElementInexistantException Si l'indice de d�calage est invalide.
     */
    private void deplacerImageActuelle(int nouvelIndice) {
        if (nouvelIndice >= 0 && nouvelIndice < getNombreImages()) {
            Image imageActuelle = getImageActuelle();
            ArrayList<T> pointsControleImageActuelle = pointsControleDesImages.get(getIndiceImageActuelle());

            pointsControleDesImages.remove(getIndiceImageActuelle());
            images.remove(getIndiceImageActuelle());

            images.add(nouvelIndice, imageActuelle);
            pointsControleDesImages.add(nouvelIndice, pointsControleImageActuelle);

            indiceImageActuelle = nouvelIndice;
            notifierObservateurs(CHANGEMENT_IMAGES);
        } else {
            throw new TentativeAccesElementInexistantException("L'indice de d�calage n'est pas valide car il ne correspond � aucune image existante avec laquelle �changer l'image � d�caler");
        }
    }
}