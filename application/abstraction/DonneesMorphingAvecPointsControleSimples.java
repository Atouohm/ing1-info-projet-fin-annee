package application.abstraction;

import java.util.ArrayList;

import exceptions.TentativeAccesElementInexistantException;

/**
 * Classe repr�sentant les donn�es utilis�es dans le processus de morphing concernant sp�cifiquement les points de contr�le simples.
 */
public class DonneesMorphingAvecPointsControleSimples extends DonneesMorphing<PointControleSimple> {

	@Override
	protected ArrayList<PointControleSimple> copierListePointsControle(ArrayList<PointControleSimple> arrayList) {
		ArrayList<PointControleSimple> copie = new ArrayList<>();
	    for (PointControle pointControle : arrayList) {
	        copie.add((PointControleSimple) pointControle.copie());
	    }
	    return copie;
	}
	
	@Override
	public PointControleSimple getPointControleImage(int indiceImage, int indicePointControle) {
		if (indiceImage >= 0 && indiceImage < getNombreImages()) {
			if (indicePointControle >= 0 && indicePointControle < getNombrePointsControle()) {
				return (PointControleSimple) pointsControleDesImages.get(indiceImage).get(indicePointControle).copie();
			} else {
				throw new TentativeAccesElementInexistantException("Point de contr�le inexistant");
			}
		} else {
			throw new TentativeAccesElementInexistantException("Image inexistante");
		}
	}
}
