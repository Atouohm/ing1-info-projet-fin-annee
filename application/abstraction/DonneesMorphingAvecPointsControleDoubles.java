package application.abstraction;

import java.util.ArrayList;

import exceptions.TentativeAccesElementInexistantException;

/**
 * Classe repr�sentant les donn�es utilis�es dans le processus de morphing concernant sp�cifiquement les points de contr�le doubles.
 */
public class DonneesMorphingAvecPointsControleDoubles extends DonneesMorphing<PointControleDouble> {

	@Override
	protected ArrayList<PointControleDouble> copierListePointsControle(ArrayList<PointControleDouble> arrayList) {
		ArrayList<PointControleDouble> copie = new ArrayList<>();
	    for (PointControle pointControle : arrayList) {
	        copie.add((PointControleDouble) pointControle.copie());
	    }
	    return copie;
	}
	
	@Override
	public PointControleDouble getPointControleImage(int indiceImage, int indicePointControle) {
		if (indiceImage >= 0 && indiceImage < getNombreImages()) {
			if (indicePointControle >= 0 && indicePointControle < getNombrePointsControle()) {
				return (PointControleDouble) pointsControleDesImages.get(indiceImage).get(indicePointControle).copie();
			} else {
				throw new TentativeAccesElementInexistantException("Point de contr�le inexistant");
			}
		} else {
			throw new TentativeAccesElementInexistantException("Image inexistante");
		}
	}
}