package application.presentation;

import application.abstraction.DonneesMorphing;
import application.abstraction.DonneesMorphingAvecPointsControleSimples;
import application.abstraction.PointControleSimple;
import application.controle.ControleAjoutPointDeControleSimple;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Cette classe abstraite repr�sente une interface pour le morphing avec des donn�es associ�es contenant des points de contr�le simples.
 */
public abstract class InterfaceMorphingAvecDonnneesMorphingAvecPointControleSimples extends InterfaceMorphing<PointControleSimple> {
    
    @Override
    protected DonneesMorphing<PointControleSimple> creerDonneesAssociee(){
        return new DonneesMorphingAvecPointsControleSimples();
    }
    
    @Override
    protected void ajouterProprieteCliquableABoiteImage(Pane boiteImage) {
        // Ajoute un contr�le pour l'ajout de points de contr�le simples � l'interface.
        boiteImage.addEventFilter(MouseEvent.MOUSE_CLICKED, new ControleAjoutPointDeControleSimple(this));
    }

    @Override
    public void mettreAJourAffichagePointsControle() {
        Pane boiteImage = getBoiteImageCentrale();

        if (boiteImage.getChildren().size() > 1) {
            boiteImage.getChildren().remove(1, boiteImage.getChildren().size());
        }

        if (donneesAssociees.uneImageEstSelectionnee()) {
            for (int i = 0 ; i < donneesAssociees.getNombrePointsControle() ; i++) {
                // Ajoute un n�ud repr�sentant le point de contr�le simple � l'interface.
                boiteImage.getChildren().add(creerUnNoeudPointControleDansBoiteImage(boiteImage, getFacteurDeZoomDesImagesAffichees(), (PointControleSimple) donneesAssociees.getPointControleImageActuelle(i), Integer.toString(i), true));
            }

            // Met � jour l'affichage des lignes entre les points de contr�le.
            mettreAJourAffichageLignesEntrePointsDeControle(boiteImage);
        }   
    }

    @Override
    protected void mettreAJourPointControleImageCentrale(String etiquette, double x, double y) {
        // Met � jour le point de contr�le simple dans les donn�es associ�es.
        getDonneesAssociees().setPointControleImageActuelle(Integer.parseInt(etiquette), new PointControleSimple(x, y), 5/getFacteurDeZoomDesImagesAffichees());
    }
}