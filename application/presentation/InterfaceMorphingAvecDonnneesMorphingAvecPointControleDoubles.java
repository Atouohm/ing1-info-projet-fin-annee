package application.presentation;

import application.abstraction.DonneesMorphing;
import application.abstraction.DonneesMorphingAvecPointsControleDoubles;
import application.abstraction.PointControleDouble;
import application.abstraction.PointControleSimple;
import application.controle.ControleAjoutPointDeControleDouble;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Cette classe abstraite repr�sente une interface pour le morphing avec des donn�es associ�es contenant des points de contr�le doubles.
 */
public abstract class InterfaceMorphingAvecDonnneesMorphingAvecPointControleDoubles extends InterfaceMorphing<PointControleDouble> {

    @Override
    protected DonneesMorphing<PointControleDouble> creerDonneesAssociee(){
        return new DonneesMorphingAvecPointsControleDoubles();
    }

    @Override
    protected void ajouterProprieteCliquableABoiteImage(Pane boiteImage) {
        // Ajoute un contr�le pour l'ajout de points de contr�le doubles � l'interface.
        ControleAjoutPointDeControleDouble controleAjoutPointDeControleDouble = new ControleAjoutPointDeControleDouble(this, boiteImage);
        boiteImage.addEventFilter(MouseEvent.MOUSE_CLICKED, controleAjoutPointDeControleDouble);
        getListeObservateurs().add(controleAjoutPointDeControleDouble);
    }

    @Override
    public void mettreAJourAffichagePointsControle() {
        Pane boiteImage = getBoiteImageCentrale();

        if (boiteImage.getChildren().size() > 1) {
            boiteImage.getChildren().remove(1, boiteImage.getChildren().size());
        }

        if (donneesAssociees.uneImageEstSelectionnee()) {
            for (int i = 0 ; i < donneesAssociees.getNombrePointsControle() ; i++) {
                PointControleDouble pointDeControle = (PointControleDouble) donneesAssociees.getPointControleImageActuelle(i);
                // Cr�e et ajoute un n�ud repr�sentant le point de contr�le double � l'interface.
                Group noeudPointDeControle1 = creerUnNoeudPointControleDansBoiteImage(boiteImage, getFacteurDeZoomDesImagesAffichees(), new PointControleSimple(pointDeControle.getX1(), pointDeControle.getY1()), i + "." + 1, true);
                Group noeudPointDeControle2 = creerUnNoeudPointControleDansBoiteImage(boiteImage, getFacteurDeZoomDesImagesAffichees(), new PointControleSimple(pointDeControle.getX2(), pointDeControle.getY2()), i + "." + 2, true);
                boiteImage.getChildren().add(noeudPointDeControle1);
                boiteImage.getChildren().add(noeudPointDeControle2);
            }

            // Met � jour l'affichage des lignes entre les points de contr�le.
            mettreAJourAffichageLignesEntrePointsDeControle(boiteImage);
            // Notifie les observateurs du changement d'affichage des points de contr�le.
            notifierObservateurs(MISE_A_JOUR_AFFICHAGE_POINTS_CONTROLE);
        }
    }

    @Override
    protected void mettreAJourAffichageLignesEntrePointsDeControle(Pane boiteImage) {
        if (boiteImage.getChildren().size() > 1 + getDonneesAssociees().getNombrePointsControle()*2) {
            boiteImage.getChildren().remove(1, boiteImage.getChildren().size() - getDonneesAssociees().getNombrePointsControle()*2);
        }

        int nombreLignesAjoutees = 1;
        for (int i = 2 ; i < boiteImage.getChildren().size() ; i += 3) {
            // Cr�e et ajoute une ligne reliant deux points de contr�le � l'interface.
            boiteImage.getChildren().add(nombreLignesAjoutees, creerLigneEntreDeuxNoeuds(boiteImage.getChildren().get(i-1), boiteImage.getChildren().get(i)));
            nombreLignesAjoutees++;
        }
    }

    @Override
    protected void mettreAJourPointControleImageCentrale(String etiquette, double x, double y) {
        String[] etiquetteDecomposee = etiquette.split("\\.");
        int numeroPointDeControleDouble = Integer.parseInt(etiquetteDecomposee[0]);
        int numeroPointDeControleSimpleDuPointDeControleDouble = Integer.parseInt(etiquetteDecomposee[1]);

        PointControleDouble pointDeControleDouble = (PointControleDouble) getDonneesAssociees().getPointControleImageActuelle(numeroPointDeControleDouble);
        if (numeroPointDeControleSimpleDuPointDeControleDouble == 1) {
            pointDeControleDouble.setX1(x);
            pointDeControleDouble.setY1(y);
        } else if (numeroPointDeControleSimpleDuPointDeControleDouble == 2) {
            pointDeControleDouble.setX2(x);
            pointDeControleDouble.setY2(y);
        }
        // Met � jour le point de contr�le double dans les donn�es associ�es.
        getDonneesAssociees().setPointControleImageActuelle(numeroPointDeControleDouble, pointDeControleDouble, 5/getFacteurDeZoomDesImagesAffichees());
    }
}