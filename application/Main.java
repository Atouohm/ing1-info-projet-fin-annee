package application;

import application.presentation.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Classe principale de l'application permettant l'initialisation et le lancement de l'application de morphing.
 */
public final class Main extends Application {
    public static Stage stage;

    /**
     * M�thode principale pour lancer l'application.
     * @param args Les arguments de la ligne de commande.
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
    	// Initialisation et configuration de l'interface graphique par d�faut de l'application.
        Main.stage = stage;
        stage.setScene((new InterfaceMorphingFormesUniesSimples()).getScene());
        stage.setTitle("G�n�rateur de morphing");
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.getIcons().add(new Image("/ressources/images/logo.png"));
        
        stage.setOnCloseRequest(event -> {
            if (!InterfaceMorphing.afficherConfirmation("Fermer l'application ?", "�tes-vous s�r de vouloir fermer l'application ? Vous perdrez tout ce que vous avez fait jusqu'� pr�sent.")) {
                event.consume(); // On annule l'�v�nement de fermeture
            } else {
                Platform.exit(); // On ferme l'application
            }
        });
        
        stage.show();
    }
}