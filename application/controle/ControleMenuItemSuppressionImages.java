package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Cette classe d�finit le contr�leur pour la suppression des images.
 */
public final class ControleMenuItemSuppressionImages extends Observateur implements EventHandler<ActionEvent> {
    public static final Integer MENU_ITEM_SUPPRESSION_IMAGE_ACTUELLE = 1;
    public static final Integer MENU_ITEM_SUPPRESSION_TOUTES_LES_IMAGES = 2;
    
    private InterfaceMorphing<?> interfaceAssociee;
    private MenuItem menuItem;
    private Integer typeDeMenuItem;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param menuItem Le MenuItem � contr�ler.
     * @param typeDeMenuItem Le type de MenuItem.
     */
    public ControleMenuItemSuppressionImages(InterfaceMorphing<?> interfaceAssociee, MenuItem menuItem, Integer typeDeMenuItem) {
        this.interfaceAssociee = interfaceAssociee;
        this.menuItem = menuItem;
        this.typeDeMenuItem = typeDeMenuItem;
    }
    
    @Override
    public void handle(ActionEvent event) {
        if (typeDeMenuItem == MENU_ITEM_SUPPRESSION_IMAGE_ACTUELLE && InterfaceMorphing.afficherConfirmation("Supprimer l'image ?", "�tes-vous s�r de vouloir supprimer l'image ? Cela supprimera �galement les points de contr�le associ�s � cette image.")) {
            interfaceAssociee.getDonneesAssociees().supprimerImageActuelle();
        } else if (typeDeMenuItem == MENU_ITEM_SUPPRESSION_TOUTES_LES_IMAGES && InterfaceMorphing.afficherConfirmation("Supprimer toutes les images ?", "�tes-vous s�r de vouloir supprimer toutes les images ? Cela supprimera �galement tous les points de contr�le.")) {
            interfaceAssociee.getDonneesAssociees().supprimerToutesLesImages();
        }
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES) {
            menuItem.setDisable(interfaceAssociee.getDonneesAssociees().getNombreImages() == 0);
        }
    }
}