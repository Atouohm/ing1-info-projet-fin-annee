package application.controle;

import application.abstraction.PointControleSimple;
import application.presentation.InterfaceMorphing;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Ce contr�leur g�re l'ajout de points de contr�le simples dans l'interface de morphing.
 */
public final class ControleAjoutPointDeControleSimple implements EventHandler<MouseEvent> {
    private InterfaceMorphing<PointControleSimple> interfaceAssociee;
    
    /**
     * Initialise le contr�leur avec l'interface de morphing associ�e.
     * @param interfaceAssociee L'interface de morphing associ�e.
     */
    public ControleAjoutPointDeControleSimple(InterfaceMorphing<PointControleSimple> interfaceAssociee) {
        this.interfaceAssociee = interfaceAssociee;
    }
    
    @Override
    public void handle(MouseEvent event) {
        if (InterfaceMorphing.boiteImageContientUneImage((Pane) event.getSource())) {
            if (event.getButton() == MouseButton.PRIMARY && InterfaceMorphing.coordonneesDansNode(((Pane) event.getSource()).getChildren().get(0), event.getSceneX(), event.getSceneY())) {
                // Ajout du point de contr�le simple
                double translationX = InterfaceMorphing.getTranslationXImageDansBoiteImage((Pane) event.getSource());
                double translationY = InterfaceMorphing.getTranslationYImageDansBoiteImage((Pane) event.getSource());
                interfaceAssociee.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleSimple((event.getX() - translationX)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees(), (event.getY() - translationY)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees()), 5/interfaceAssociee.getFacteurDeZoomDesImagesAffichees());
            }
        }
    }
}