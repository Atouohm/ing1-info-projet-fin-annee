package application.controle;

import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import java.io.File;
import application.abstraction.DonneesMorphing;
import application.abstraction.DonneesMorphingAvecPointsControleDoubles;
import application.abstraction.DonneesMorphingAvecPointsControleSimples;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import morphing.Morphing;
import morphing.MorphingFormesUniesSimples;
import morphing.MorphingFormesUniesArrondies;
import morphing.MorphingImages;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

/**
 * Ce contr�leur g�re le bouton pour g�n�rer le morphing.
 */
public final class ControleBoutonGenererMorphing extends Observateur implements EventHandler<ActionEvent> {
    public static final Integer MORPHING_FORMES_UNIES_SIMPLES = 1;
    public static final Integer MORPHING_FORMES_UNIES_ARRONDIES = 2;
    public static final Integer MORPHING_IMAGES = 3;
    
    private InterfaceMorphing<?> interfaceAssociee;
    private Button bouton;
    private Integer typeDeMorphing;
    
    /**
     * Initialise le contr�leur avec l'interface de morphing associ�e, le bouton et le type de morphing.
     * @param interfaceAssociee L'interface de morphing associ�e.
     * @param bouton Le bouton de g�n�ration du morphing.
     * @param typeDeMorphing Le type de morphing.
     */
    public ControleBoutonGenererMorphing(InterfaceMorphing<?> interfaceAssociee, Button bouton, Integer typeDeMorphing) {
        this.interfaceAssociee = interfaceAssociee;
        this.bouton = bouton;
        this.typeDeMorphing = typeDeMorphing;
    }
    
    @Override
    public void handle(ActionEvent e) {
        interfaceAssociee.mettreAJourAffichagePointsControle();
        
        final Morphing<?> morphing;
        if (typeDeMorphing == MORPHING_FORMES_UNIES_SIMPLES) {
            morphing = new MorphingFormesUniesSimples((DonneesMorphingAvecPointsControleSimples) interfaceAssociee.getDonneesAssociees(), interfaceAssociee.getNombreImagesIntermediaires());
        } else if (typeDeMorphing == MORPHING_FORMES_UNIES_ARRONDIES) {
            morphing = new MorphingFormesUniesArrondies((DonneesMorphingAvecPointsControleSimples) interfaceAssociee.getDonneesAssociees(), interfaceAssociee.getNombreImagesIntermediaires());
        } else if (typeDeMorphing == MORPHING_IMAGES) {
            morphing = new MorphingImages((DonneesMorphingAvecPointsControleDoubles) interfaceAssociee.getDonneesAssociees(), interfaceAssociee.getNombreImagesIntermediaires());
        } else {
            morphing = null;
        }
        
        if (morphing != null) {
            InterfaceMorphing.executerLongueTache(
                    () -> {
                        morphing.genererImagesResultat();
                    },
                    () -> {
                        afficherFenetreModaleImagesResultat(morphing);
                    },
                    (StackPane) ((HBox) bouton.getParent().getParent()).getChildren().get(1)
            );
        }
    }
    
    /**
     * Affiche la fen�tre modale des images r�sultat du morphing.
     * @param morphing Le morphing.
     */
    private void afficherFenetreModaleImagesResultat(Morphing<?> morphing) {
        // Cr�ation de la bo�te d'image
        Pane boiteImage = new Pane();
        ControleChangementTailleBoiteImage controleChangementDeTailleBoiteImage = new ControleChangementTailleBoiteImage(null, boiteImage);
        boiteImage.widthProperty().addListener(controleChangementDeTailleBoiteImage);
        boiteImage.heightProperty().addListener(controleChangementDeTailleBoiteImage);
        boiteImage.setMinWidth(1);
        boiteImage.setMinHeight(1);
        
        // Configuration de l'image
        ImageView imageView = new ImageView();
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(boiteImage.widthProperty());
        imageView.fitHeightProperty().bind(boiteImage.heightProperty());
        imageView.setImage(morphing.getImageResultat(0));
        boiteImage.getChildren().add(imageView);
        
        // Configuration du slider
        Slider slider = new Slider();
        slider.setMin(1);
        slider.setMax(morphing.getNombreImagesResultat());
        slider.setValue(1); 
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(0);
        slider.setSnapToTicks(true);
        ControleSlider controleSlider = new ControleSlider(imageView, morphing);
        slider.valueProperty().addListener(controleSlider);
        slider.setPadding(new Insets(10, 0, 0, 0));
        
        // Configuration de la l�gende
        Label legendeLabel = new Label("Images r�sultat");
        legendeLabel.getStyleClass().add("legende");
        
        // Configuration du bouton de t�l�chargement du GIF
        Button boutonTelecharger = new Button("T�l�charger le GIF");
        boutonTelecharger.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Sauvegarder le GIF");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Fichier GIF (*.gif)", "*.gif"));
            
            File file = fileChooser.showSaveDialog(null);
            if (file != null) {
                InterfaceMorphing.executerLongueTache(
                        () -> {
                            boutonTelecharger.setDisable(true);
                            morphing.sauvegarderGIF(file.getAbsolutePath(), interfaceAssociee.getDureeGIF());
                        },
                        () -> {
                            boutonTelecharger.setDisable(false);
                        },
                        (StackPane) ((HBox) boutonTelecharger.getParent().getParent()).getChildren().get(1)
                );
            }
        });
        
        // Configuration du bandeau de t�l�chargement
        HBox bandeauTelecharger = new HBox(new StackPane(boutonTelecharger), new StackPane());
        bandeauTelecharger.setAlignment(javafx.geometry.Pos.CENTER);
        
        // Configuration du bandeau du bas
        VBox bandeauBas = new VBox(new StackPane(slider), new StackPane(legendeLabel), bandeauTelecharger);
        bandeauBas.setSpacing(10);
        
        // Cr�ation de la partie centrale de la fen�tre modale
        BorderPane partieImageCentrale = new BorderPane();
        partieImageCentrale.setCenter(boiteImage);
        partieImageCentrale.setBottom(bandeauBas);
        partieImageCentrale.setPrefWidth(600);
        partieImageCentrale.setPrefHeight(550);
        
        // Affichage de la fen�tre modale
        InterfaceMorphing.afficherFenetreModale("Images r�sultat", partieImageCentrale, true);
    }

    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_POINTS_DE_CONTROLE && (typeDeMorphing == MORPHING_FORMES_UNIES_SIMPLES || typeDeMorphing == MORPHING_FORMES_UNIES_ARRONDIES)) {
            Boolean pointsDeControleBienPlaces = true;
            for (int i = 0 ; i < interfaceAssociee.getDonneesAssociees().getNombreImages() ; i++) {
                if (interfaceAssociee.getDonneesAssociees().getNombrePointsControle() < 2 || !interfaceAssociee.getDonneesAssociees().getPointControleImage(i, 0).equals(interfaceAssociee.getDonneesAssociees().getPointControleImage(i, interfaceAssociee.getDonneesAssociees().getNombrePointsControle()-1))) {
                    pointsDeControleBienPlaces = false;
                }
            }
            
            if (pointsDeControleBienPlaces && interfaceAssociee.getDonneesAssociees().getNombreImages() >= 2) {
                bouton.setDisable(false);
            } else {
                bouton.setDisable(true);
            }
        } else if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES && typeDeMorphing == MORPHING_IMAGES) {
            bouton.setDisable(!(interfaceAssociee.getDonneesAssociees().getNombreImages() >= 2));
        }
    }
}
