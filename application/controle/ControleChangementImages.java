package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;

/**
 * Cette classe d�finit le contr�leur pour le changement d'images.
 */
public final class ControleChangementImages extends Observateur {
    private InterfaceMorphing<?> interfaceAssociee;

    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     */
    public ControleChangementImages(InterfaceMorphing<?> interfaceAssociee) {
        this.interfaceAssociee = interfaceAssociee;
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES || typeDeModification == DonneesMorphing.CHANGEMENT_INDICE_IMAGE_ACTUELLE) {
            interfaceAssociee.mettreAJourAffichageImageCentrale();
            interfaceAssociee.mettreAJourAffichagePointsControle();

            if (interfaceAssociee.uneImageEstAffichee()) {
                interfaceAssociee.setFacteurZoomDeImageAffichee();
            }
        }
    }
}