package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Cette classe d�finit le contr�leur pour le d�calage de l'image actuelle.
 */
public final class ControleMenuItemDecalageImageActuelle extends Observateur implements EventHandler<ActionEvent> {
    public static final Integer MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_DEBUT = 1;
    public static final Integer MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_FIN = 2;
    
    private InterfaceMorphing<?> interfaceAssociee;
    private MenuItem menuItem;
    private Integer typeDeMenuItem;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param menuItem Le MenuItem � contr�ler.
     * @param typeDeMenuItem Le type de MenuItem.
     */
    public ControleMenuItemDecalageImageActuelle(InterfaceMorphing<?> interfaceAssociee, MenuItem menuItem, Integer typeDeMenuItem) {
        this.interfaceAssociee = interfaceAssociee;
        this.menuItem = menuItem;
        this.typeDeMenuItem = typeDeMenuItem;
    }
    
    @Override
    public void handle(ActionEvent event) {
        if (typeDeMenuItem == MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_DEBUT) {
            interfaceAssociee.getDonneesAssociees().decalerImageActuelleVersDebut();
        } else if (typeDeMenuItem == MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_FIN) {
            interfaceAssociee.getDonneesAssociees().decalerImageActuelleVersFin();
        }
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES || typeDeModification == DonneesMorphing.CHANGEMENT_INDICE_IMAGE_ACTUELLE) {
            if (typeDeMenuItem == MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_DEBUT) {
                menuItem.setDisable(!interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee() || interfaceAssociee.getDonneesAssociees().getIndiceImageActuelle() == 0);
            } else if (typeDeMenuItem == MENU_ITEM_DECALAGE_IMAGE_ACTUELLE_VERS_FIN) {
                menuItem.setDisable(!interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee() || interfaceAssociee.getDonneesAssociees().getIndiceImageActuelle() == interfaceAssociee.getDonneesAssociees().getNombreImages()-1);
            }
        }
    }
}