package application.controle;

import application.abstraction.Observable;
import application.abstraction.PointControleDouble;
import application.abstraction.PointControleSimple;
import application.presentation.InterfaceMorphing;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Ce contr�leur g�re l'ajout de points de contr�le doubles dans l'interface de morphing.
 */
public final class ControleAjoutPointDeControleDouble extends Observateur implements EventHandler<MouseEvent> {
    private InterfaceMorphing<PointControleDouble> interfaceAssociee;
    private Pane boiteImageAssociee;
    private PointControleSimple premierClic;
    
    /**
     * Initialise le contr�leur avec l'interface de morphing et la zone d'image associ�e.
     * @param interfaceAssociee L'interface de morphing associ�e.
     * @param boiteImageAssociee La zone d'image associ�e.
     */
    public ControleAjoutPointDeControleDouble(InterfaceMorphing<PointControleDouble> interfaceAssociee, Pane boiteImageAssociee) {
        this.interfaceAssociee = interfaceAssociee;
        this.boiteImageAssociee = boiteImageAssociee;
        premierClic = null;
    }
    
    /**
     * R�cup�re le premier clic effectu� par l'utilisateur.
     * @return Le premier clic.
     */
    public PointControleSimple getPremierClic() {
        return premierClic;
    }
    
    /**
     * D�finit le premier clic effectu� par l'utilisateur.
     * @param premierClic Le premier clic � d�finir.
     */
    public void setPremierClic(PointControleSimple premierClic) {
        this.premierClic = premierClic;
    }
    
    @Override
    public void handle(MouseEvent event) {
        if (InterfaceMorphing.boiteImageContientUneImage((Pane) event.getSource())) {
            if (event.getButton() == MouseButton.PRIMARY && InterfaceMorphing.coordonneesDansNode(boiteImageAssociee.getChildren().get(0), event.getSceneX(), event.getSceneY())) {
                double translationX = InterfaceMorphing.getTranslationXImageDansBoiteImage(boiteImageAssociee);
                double translationY = InterfaceMorphing.getTranslationYImageDansBoiteImage(boiteImageAssociee);
                
                if (getPremierClic() == null) {
                    // Premier clic : ajout d'un point simple
                    setPremierClic(new PointControleSimple((event.getX() - translationX)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees(), (event.getY() - translationY)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees()));
                    boiteImageAssociee.getChildren().add(interfaceAssociee.creerUnNoeudPointControleDansBoiteImage(boiteImageAssociee, interfaceAssociee.getFacteurDeZoomDesImagesAffichees(), getPremierClic(), interfaceAssociee.getDonneesAssociees().getNombrePointsControle() + ".1", false));
                    InterfaceMorphing.desactiverEvenements(boiteImageAssociee.getChildren().subList(1, boiteImageAssociee.getChildren().size()-1));
                } else {
                    // Deuxi�me clic : ajout du point double et r�activation des �v�nements
                    PointControleSimple premierClicCopie = getPremierClic();
                    setPremierClic(null);
                    interfaceAssociee.getDonneesAssociees().ajouterPointControleImageActuelle(new PointControleDouble(premierClicCopie.getX(), premierClicCopie.getY(), (event.getX() - translationX)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees(), (event.getY() - translationY)/interfaceAssociee.getFacteurDeZoomDesImagesAffichees()), 5/interfaceAssociee.getFacteurDeZoomDesImagesAffichees());
                    InterfaceMorphing.reactiverEvenements(boiteImageAssociee);
                }
            }
        }
    }

    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == InterfaceMorphing.MISE_A_JOUR_AFFICHAGE_POINTS_CONTROLE) {
            // R�initialisation du premier clic apr�s modification
            setPremierClic(null);
        }
    }
}