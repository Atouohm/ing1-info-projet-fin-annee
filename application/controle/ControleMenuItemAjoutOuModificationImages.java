package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import java.io.File;
import javafx.stage.FileChooser;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;

/**
 * Cette classe d�finit le contr�leur pour le menu item d'ajout ou de modification d'images.
 */
public final class ControleMenuItemAjoutOuModificationImages extends Observateur implements EventHandler<ActionEvent> {
    public static final Integer MENU_ITEM_AJOUT_IMAGE = 1;
    public static final Integer MENU_ITEM_MODIFICATION_IMAGE_ACTUELLE = 2;
    
    private InterfaceMorphing<?> interfaceAssociee;
    private MenuItem menuItem;
    private Integer typeDeMenuItem;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param menuItem Le MenuItem � contr�ler.
     * @param typeDeMenuItem Le type de MenuItem.
     */
    public ControleMenuItemAjoutOuModificationImages(InterfaceMorphing<?> interfaceAssociee, MenuItem menuItem, Integer typeDeMenuItem) {
        this.interfaceAssociee = interfaceAssociee;
        this.menuItem = menuItem;
        this.typeDeMenuItem = typeDeMenuItem;
    }
    
    @Override
    public void handle(ActionEvent event) {
        if (typeDeMenuItem == MENU_ITEM_AJOUT_IMAGE) {
            String urlNouvelleImage = demanderUneImage("Choisir une image � ajouter");
            if (urlNouvelleImage != null && interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee() && imagesPasDeMemeTaille(new Image(urlNouvelleImage), interfaceAssociee.getDonneesAssociees().getImageActuelle())) {
                InterfaceMorphing.afficherAlerte("L'image ajout�e doit avoir la m�me taille que les images d�j� charg�es.");
            } else if (urlNouvelleImage != null) {
                interfaceAssociee.getDonneesAssociees().ajouterImage(urlNouvelleImage);
            }
        } else if (typeDeMenuItem == MENU_ITEM_MODIFICATION_IMAGE_ACTUELLE && (interfaceAssociee.getDonneesAssociees().getNombreImages() > 1 || InterfaceMorphing.afficherConfirmation("Modifier la derni�re image restante ?", "�tes-vous s�r de vouloir modifier la derni�re image restante ? Cela supprimera �galement tous les points de contr�le."))) {
            String urlNouvelleImage = demanderUneImage("Choisir une nouvelle image");
            if (urlNouvelleImage != null && interfaceAssociee.getDonneesAssociees().getNombreImages() > 1 && interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee() && imagesPasDeMemeTaille(new Image(urlNouvelleImage), interfaceAssociee.getDonneesAssociees().getImageActuelle())) {
                InterfaceMorphing.afficherAlerte("L'image modifi�e doit avoir la m�me taille que les images d�j� charg�es.");
            } else if (urlNouvelleImage != null) {
                interfaceAssociee.getDonneesAssociees().setImageActuelle(urlNouvelleImage);
            }
        }
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES && typeDeMenuItem == MENU_ITEM_MODIFICATION_IMAGE_ACTUELLE) {
            menuItem.setDisable(!interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee());
        }
    }
    
    /**
     * M�thode pour demander � l'utilisateur de choisir une image.
     * @param message Le message � afficher dans la bo�te de dialogue.
     * @return L'URL de l'image choisie.
     */
    private static String demanderUneImage(String message) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(message);
        
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Fichier image", "*.png", "*.jpg");
        fileChooser.getExtensionFilters().add(filter);
        
        File selectedFile = fileChooser.showOpenDialog(null);
        
        if (selectedFile != null) {
            return selectedFile.toURI().toString();
        } else {
            return null;
        }
    }
    
    /**
     * M�thode pour v�rifier si deux images ont des tailles diff�rentes.
     * @param image1 La premi�re image.
     * @param image2 La deuxi�me image.
     * @return true si les images ont des tailles diff�rentes, sinon false.
     */
    private static boolean imagesPasDeMemeTaille(Image image1, Image image2) {
        return image1 != null && image2 != null && (image1.getWidth() != image2.getWidth() || image1.getHeight() != image2.getHeight());
    }
}
