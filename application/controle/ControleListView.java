package application.controle;

import application.abstraction.DonneesMorphing;
import application.abstraction.Observable;
import application.presentation.InterfaceMorphing;
import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * Cette classe d�finit le contr�leur pour la ListView.
 */
public final class ControleListView extends Observateur implements EventHandler<MouseEvent> {
    private InterfaceMorphing<?> interfaceAssociee;
    private ListView<String> listView;
    
    /**
     * Constructeur de la classe.
     * @param interfaceAssociee L'interface morphing associ�e.
     * @param listView La ListView � contr�ler.
     */
    public ControleListView(InterfaceMorphing<?> interfaceAssociee, ListView<String> listView) {
        this.interfaceAssociee = interfaceAssociee;
        this.listView = listView;
    }
    
    @Override
    public void handle(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY && !listView.getItems().isEmpty()) {
            interfaceAssociee.getDonneesAssociees().setIndiceImageActuelle(listView.getSelectionModel().getSelectedIndex());
        }
    }
    
    @Override
    public void traitementApresModification(Observable observable, Integer typeDeModification) {
        if (typeDeModification == DonneesMorphing.CHANGEMENT_IMAGES) {
            listView.getItems().clear();
            for (int i = 1; i <= interfaceAssociee.getDonneesAssociees().getNombreImages(); i++) {
                listView.getItems().add("Image " + i);
            }
            
            if (interfaceAssociee.getDonneesAssociees().uneImageEstSelectionnee()) {
                listView.getSelectionModel().select(interfaceAssociee.getDonneesAssociees().getIndiceImageActuelle());
            }
        } else if (typeDeModification == DonneesMorphing.CHANGEMENT_INDICE_IMAGE_ACTUELLE) {
            listView.getSelectionModel().select(interfaceAssociee.getDonneesAssociees().getIndiceImageActuelle());
        }
    }
}