package application.controle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.ImageView;
import morphing.Morphing;

/**
 * Cette classe d�finit le contr�leur pour le changement de valeur d'un slider.
 */
public final class ControleSlider implements ChangeListener<Number> {
    private ImageView imageView;
    private Morphing<?> morphing;
    
    /**
     * Constructeur de la classe.
     * @param imageView L'ImageView associ�.
     * @param morphing L'objet Morphing associ�.
     */
    public ControleSlider(ImageView imageView, Morphing<?> morphing) {
        this.imageView = imageView;
        this.morphing = morphing;
    }
    
    @Override
    public void changed(ObservableValue<? extends Number> valeurObservable, Number ancienneValeur, Number nouvelleValeur) {
        int ancienneValeurArrondie = (int) Math.round(ancienneValeur.doubleValue());
        int nouvelleValeurArrondie = (int) Math.round(nouvelleValeur.doubleValue());
        
        if (ancienneValeurArrondie != nouvelleValeurArrondie) {
            imageView.setImage(morphing.getImageResultat(nouvelleValeurArrondie-1));
        }
    }
}